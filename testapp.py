import unittest
import time
from functools import partial
from kivy.clock import Clock

from simpleapp import SimpleApp


class Test(unittest.TestCase):

    def pause(*args):
        time.sleep(0.000001)

    def run_test(self, app, *args):
        self.assertEqual(app.text, "Hello World")
        app.stop()

    def test_example(self):
        app = SimpleApp()
        p = partial(self.run_test, app)
        Clock.schedule_once(p, 0.000001)
        app.run()


if __name__ == '__main__':
    unittest.main()